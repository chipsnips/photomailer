# Imports code from libraries developed by other programmers.
# Without these, our program would be very complex and take 
# a very long time to make.
from lib.email_sender import EmailSender
from picamera import PiCamera
from easygui import *
import time
import sys

PICNAME = "picture.jpg"  # Files save as jpeg, use .jpg extention
PREVIEW = 5              # Preview time is in seconds.

#####################################################################################
#
# Subroutine name: take_a_picture()
#
# Description: This is a subroutine that takes a picture using the PiCamera class.
#
# Input(s):
#     picture_name: The file name you want your picture called
#     preview_time: Optional keyword parameter that defaults to 0 so the picture is
#                   taken immediately.
# Output(s):
#     Leaves a picture named with the value of the picture_name variable in the
#     local directory (where this script is ran from.
#
# Returns:
#     Nothing
#
#####################################################################################
def take_a_picture(picture_name, preview_time=0):

    # Set a flag to indicate if the calling code wants to give the viewer
    #  time to look at their pose (or make the perfect crazy face)
    if preview_time is 0 or preview_time is False:
        do_preview = False
    else:
        do_preview = True

    camera = PiCamera()  # Create a camera object from the camera class

    # If the calling code sends a preview time, start the preview and show it
    # for "preview_time" seconds.
    if do_preview:
        camera.start_preview()
        time.sleep(preview_time)

    camera.capture(picture_name)

    if do_preview:
        camera.stop_preview()

    camera.close()

#####################################################################################
#
# Subroutine name: create_thumbnail()
#
# Description: Creates a smaller version of a picture.  May be used to display in a
#              user interface box.
#
# Input(s):
#     picture_name: The file name of the picture you want to shrink
#     width: Optional keyword parameter that defaults to 400.  It is the width
#            in pixels we want the shrunken picture to be.  The hieght is scaled
#            to maintain the correct ratio.
#
# Output(s):
#     Leaves a picture named with the value of the picture_name variable with "th_"
#     in the local directory 
#
# Returns:
#     Name of thumbnail picture
#
#####################################################################################
def create_thumbnail(picture_name, width=400):

    from PIL import Image

    original_image = Image.open(picture_name)
    w, h = original_image.size
    original_image.thumbnail((width, h), Image.ANTIALIAS)
    thumbnailName = "th_" + picture_name
    original_image.save(thumbnailName)
    original_image.close()

    return thumbnailName

#####################################################################################
#
# Subroutine name: send_picture_by_email()
#
# Description: Creates an EmailSender object and sends an email with pre-defined
#              address, subject, body, and attachment.
#
# Input(s):
#     email_address: Text email address to send attachment to
#     attachment:  Text name of the attachment you want to send
#     count: A number to add to the subject to keep photos separate
#
# Output(s):
#     Sends an email with attachment to the email address
#
# Returns:
#     Nothing
#
#####################################################################################
def send_picture_by_email(email_address, attachment, photo_count):

    email = EmailSender(from_address='<email address to use goes here>', password='<password goes here>')
    email.to_address = email_address
    email.subject = "Raspberry Pi Photo Booth Picture #" + str(photo_count)
    email.body = "See attached image"
    email.attachment = attachment
    email.send()

# Program starts here!
title = "Photo Booth"
msg = 'Welcome, press "Take a Picture"'
choices = ["Take a Picture", "Exit"]
reply = buttonbox(msg, choices=choices)

if reply == "Exit":
    print "User chose to exit :("
    sys.exit()

elif reply == "Take a Picture":
    print "User chose to take a picture!"
    take_a_picture(PICNAME, preview_time=PREVIEW)
