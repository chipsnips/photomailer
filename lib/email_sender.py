import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders

class EmailSender:

    from_address = None
    password = None
    to_address = None
    subject = None
    attachment = None
    body = None

    def __init__(self, from_address=None, password=None):
        self.from_address = from_address
        self.password = password

    def set_subject(self, subject):
        self.subject = subject

    def set_body(self, body):
        self.body = body

    def set_from_address(self, address):
        self.from_address = address

    def set_to_address(self, address):
        self.to_address = address

    def set_attachment(self, attachment):
        self.attachment = attachment

    def send(self):
         
        msg = MIMEMultipart()
         
        msg['From'] = self.from_address
        msg['To'] = self.to_address
        msg['Subject'] = self.subject
         
        body = self.body
         
        msg.attach(MIMEText(self.body, 'plain'))
         
        attachment = open(self.attachment, "rb")
         
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % self.attachment)
         
        msg.attach(part)
         
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(self.from_address, self.password)
        text = msg.as_string()
        server.sendmail(self.from_address, self.to_address, text)
        server.quit()
